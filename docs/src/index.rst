.. SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
.. SPDX-License-Identifier: MIT


Welcome to Sample Calculator's documentation!
#############################################

Sample Calculator is a command line tool to calculate characteristic values of a sample. 

It provides the following features:

* Reading samples from command line and CSV (Colon Separated Values) files.
* Performing statistic calculations such as average, variance, and deviation.
* Configurable logging of results and interim results.
* Easy integration of new input sources and calculations.

Here you can find the following information:

* The :doc:`user-guide` describes how to install and use the Sample Calculator command line tool.
* The :doc:`developer-guide` explains how you can contribute to the further development of the SampleCalculator.


.. toctree:: 
   :hidden:

   user-guide
   developer-guide
   modules

Indices and tables:
*******************

* :ref:`genindex`
* :ref:`search`
